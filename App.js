//variabels

const start = document.querySelector("#start");
const stop = document.querySelector("#stop");
const reset = document.querySelector("#reset");

//funstions

const startTimer = () => {
  let seconds = 0;
  let minutes = 0;
  let hours = 0;

  let myTimer = setInterval(() => {
    seconds += 1;
    document.querySelector(".seconds").textContent = seconds;
    if (seconds == 59) {
      seconds = -1;

      setTimeout(() => {
        minutes += 1;
        document.querySelector(".minutes").textContent = minutes;
      }, 1000);
    }

    if (minutes == 59) {
      minutes = -1;

      setTimeout(() => {
        hours += 1;
        document.querySelector(".hours").textContent = hours;
      }, 59000);
    }
  }, 1000);

  stop.addEventListener("click", () => {
    clearInterval(myTimer);
  });
  reset.addEventListener("click", () => {
    clearInterval(myTimer);
    document.querySelector(".seconds").textContent = 0;
    document.querySelector(".minutes").textContent = 0;
    document.querySelector(".hours").textContent = 0;
  });
};

//EventListeners
Listeners();
function Listeners() {
  start.addEventListener("click", startTimer);
}
